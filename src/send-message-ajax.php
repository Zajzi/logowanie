<?php

require_once './includes/session.php';

try {
    sendMessage((int) $_POST['conversationId'], $_POST['message']);
} catch (JsonException $e) {
    echo $e->getMessage();
}
