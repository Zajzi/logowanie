<?php

require_once './includes/session.php';

try {
    $messages = getMessages();
} catch (JsonException $e) {
    echo $e->getMessage();
    die;
}
$conversationMessages = array_filter($messages, fn($value) => $value['chat_id'] == $_GET['conversationId']);

if ($conversationMessages) {
    usort($conversationMessages, fn($item1, $item2) => $item1['id'] <=> $item2['id']);
    $lastMessageId = array_slice($conversationMessages, -1)[0]['id'];
}

?>
<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $_GET['conversationName'] ?></title>
    <?php require_once 'libs.php' //jQuery etc. ?>
    <link rel="stylesheet" href="assets/conversation.css">
</head>
<body>

<div id="conversation">
    <?php

    foreach ($conversationMessages as $message) {

        list($date, $time) = explode(' ', $message['date']); //Explode to date and time
        $avatar = getUserAvatar($message['login']);
        if (!$avatar) {
            $avatar = '';
        }

        //Check if the message is from the logged-in user
        if ($message['login'] === $_SESSION['login']) {

            $html = <<<HTML
            <div class="container darker"><span class="time-left">{$date}</span><br>
            <img src="{$avatar}" alt="Avatar" class="right" style="width:100%;">
            <p>{$message['message']}</p><br>
            <span class="time-left">{$time}</span>
            <span class="time-right">{$message['login']}</span>
            </div>
            HTML;
        } else {
            $html = <<<HTML
            <div class="container"><span class="time-right">{$date}</span><br>
            <img src="{$avatar}" alt="Avatar" style="width:100%;">
            <p>{$message['message']}</p><br>
            <span class="time-right">{$time}</span>
            <span class="time-left"> &nbsp;{$message['login']}</span>
            </div>
            HTML;
        }
        echo $html;
    }
    ?>
</div>

<form>
    <input id="message" class="container" type="text" autocomplete="off" placeholder="Wiadomość..."
           style="border-color: #adaebf!important;">

    <input id="conversationId" name="conversationId" type="hidden" value="<?php echo $_GET['conversationId']; ?>">
    <input id="submit" class="container darker" type="button" value="Wyślij" onclick="sendMessage();">
</form>

<a href="chat-query.php?leaveConversationId=<?php echo $_GET['conversationId']; ?>&conversationWindow=1"
   onclick="return confirm('Napewno chcesz usunąć siebie z tej konwersacji?')">
    <button id="leaveConversation" class="container darker">
        Usuń siebie z konwersacji
    </button>
</a>

<hr>
<ul id="users">
    <!-- Users online loaded with jQuery -->
</ul>
<script>

</script>

<script>
    $(document).ready(function () {
        //Prevent the form from being sent by default
        $('form').on('submit', function (e) {
            e.preventDefault();
        });

        getMessages();

        //Scroll to the bottom of page when document is ready
        scrollToBottom(1000);

        showUsersWithStatus();
    });

    //Scroll to the bottom
    function scrollToBottom(miliseconds) {
        $('html, body').animate({
            scrollTop: $("#leaveConversation").offset().top
        }, miliseconds);
    }

    //Get user avatar
    function getAvatar(login) {
        let result = '';
        $.ajax({
            url: 'chat-query.php',
            type: 'post',
            async: false,
            datatype: "text",
            data: {
                "getUserAvatar": "1",
                "login": login
            }
        }).done(function (response) {
            result = response;
        });
        return result;
    }

    //Enter-key pressed -> send message
    $('#message').on('keypress', function (e) {
        if (e.which == 13) {
            sendMessage();
        }
    });

    //Send message
    function sendMessage() {
        let message = $('#message').val();
        let conversationId = $('#conversationId').val();
        $.ajax({
            type: "post",
            url: "send-message-ajax.php",
            data: {
                'message': message,
                'conversationId': conversationId
            },
        });
        $('#message').val(''); //Clear input
        scrollToBottom(1000); //Scroll once more
    }

    let messagesArrived = 0;
    let title = document.title;

    function changeTitle(messagesArrived) {
        document.title = '(' + messagesArrived + ') ' + title;
    }

    //Check if the last message id exist (necessary for the api call)
    let lastMessageId = <?php echo $lastMessageId ?? 0 ?>;

    function getMessages() {
        //Change to the original title if the conversation is focused
        if (document.hasFocus()) {
            document.title = title;
            messagesArrived = 0;
        }

        $.ajax({
            type: "post",
            url: "get-new-message-ajax.php",
            data: {
                'lastMessageId': lastMessageId,
                'conversationId': <?php echo $_GET['conversationId'] ?>
            },
            datatype: "json",
            success: function (messagesResponse) {
                let messages = [];
                $.each(JSON.parse(messagesResponse), function (key, val) {

                    lastMessageId = val.id > lastMessageId ? val.id : lastMessageId; //look for the highest message id
                    let row = '';
                    let datetime = val.date.split(" ");
                    let date = datetime[0];
                    let time = datetime[1];

                    if (val.login === '<?php echo $_SESSION['login'] ?>') {
                        row = `<div class="container darker"><span class="time-left">${date}</span><br>
                            <img src="${getAvatar(val.login)}" alt="Avatar" class="right" style="width:100%;">
                            <p>${val.message}</p><br>
                            <span class="time-left">${time}</span>
                            <span class="time-right">${val.login}</span>
                            </div>`;
                    } else {
                        row = `<div class="container"><span class="time-right">${date}</span><br>
                            <img src="${getAvatar(val.login)}" alt="Avatar" style="width:100%;">
                            <p>${val.message}</p><br>
                            <span class="time-right">${time}</span>
                            <span class="time-left"> &nbsp;${val.login}</span>
                            </div>`;
                    }
                    messages.push(row);
                });
                //If new messages were found
                if (messages.length > 0) {
                    $('#conversation').append(messages);
                    scrollToBottom(1000);

                    //Change the document title for the notifications
                    messagesArrived += messages.length;
                    if (!document.hasFocus()) {
                        changeTitle(messagesArrived);
                    }
                }
            }
        });
        setTimeout(getMessages, 1800);
    }


    //Show users + status
    function showUsersWithStatus() {
        $.ajax({
            type: "post",
            url: "users-with-status-ajax.php",
            data: {
                'conversationId': <?php echo $_GET['conversationId']; ?>
            },
            datatype: "json",
            success: function (response) {
                let html = ``;

                $.each(JSON.parse(response), function (login, isOnline) {
                    html += `
                        <li style=${isOnline > 0 ? 'color:green' : 'color:red'}>
                        <span class="black-font">${login}</span>
                        </li>`;
                });

                $('#users').html(html);
            }
        });
        setTimeout(showUsersWithStatus, 1000);
    }

</script>
</body>
</html>
