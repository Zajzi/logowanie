<?php

require_once '../vendor/autoload.php';
require_once './includes/session.php';

use Zad3\Classes\Database;

$db = new Database();

if ($db->userIsLogged()) {
    redirectTo('index', 'Wyloguj aby się zarejestrować. Trwa przekierowanie...');
    die;
}

?>

<!DOCTYPE html>
<html lang="pl">
<body style="margin: 20px auto; width: 85%; font-size: 125%">

<form action="register-process.php" method="post">
    <?php
    if (isset($_GET['error'])) {
        echo '<label style="color:red;">' . $_GET['error'] . '</label><br />';
    }
    ?>
    <label for="login">Login(*):</label><br />
    <input type="text" name="login" /><br /><br />

    <label for="password">Hasło(*):</label><br />
    <input type="password" name="password" /><br /><br />

    <label for="password-repeat">Powtórz hasło(*):</label><br />
    <input type="password" name="password-repeat" /><br /><br />

    <label for="email">Email:</label><br />
    <input type="email" name="email" /><br /><br />

    <label for="age">Wiek:</label><br />
    <input type="number" name="age" /><br /><br />

    <label for="phone">Telefon:</label><br />
    <input type="tel" name="phone" /><br /><br />

    <label for="city">Miejscowość:</label><br />
    <input type="text" name="city" /><br /><br />

    <span style="font-size: 70%">Pola oznaczone gwiazdką (*) są obowiązkowe.</span><br /><br />
    <input type="submit" value="Zarejestruj się" style="width: 8em; height: 3em" />
</form>

</body>
</html>
