<?php

require_once '../vendor/autoload.php';
require_once './includes/session.php';

use Zad3\Classes\Database;
use Zad3\Classes\User;
use Zad3\Classes\UserDAO;

$db = new Database();
$userDAO = new UserDAO($db);

//Simple access restriction for requests other than POST
if (empty($_POST) || $db->userIsLogged()) {
    redirectTo('register-form', 'Nieuprawniony dostęp. Trwa przekierowanie...');
    die;
}

$login = trim($_POST['login']);
$password = $_POST['password'];

/**
 * @param $login
 * @param $password
 * @return mixed
 * @throws JsonException
 */
function addUserToApi($login, $password)
{
    $ch = curl_init();

    $options = [
        CURLOPT_URL => 'http://tank.iai-system.com/api/user/add',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => 'login=' . $login . '&password=' . $password,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_SSL_VERIFYHOST => 0,
    ];
    curl_setopt_array($ch, $options);

    $json = curl_exec($ch);
        try {
        $result = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
    } catch (JsonException $e) {
//        Rethrow JsonException
        throw new JsonException('JsonException: ' . $e->getMessage()
            . '. [JsonVariable]: ' . print_r($json, true));
    }
    curl_close($ch);
    return $result;
}

try {
    //Empty login or password -> redirect back to the login
    if (empty($login) || empty($password)) {
        throw new Exception('Login lub hasło jest puste!');
    }

    //The passwords don't match
    if ($password !== $_POST['password-repeat']) {
        throw new Exception('Podane hasła nie są identyczne!');
    }

    try {
        addUserToApi($login, $password);
    } catch (JsonException $e) {
        //Re-throw
        throw new JsonException($e->getMessage());
    }

    $user = new User($login, $password, $_POST['email'], $_POST['age'], $_POST['phone'], $_POST['city']);
    $userDAO->addUser($user);


} catch (PDOException $e) {
    echo $e->getMessage();
    die;
} catch (JsonException $e) {
    echo 'Nie udało się dodać użytkownika do Api. ' . $e->getMessage();
    die;
} catch (Exception $e) {
    echo $e->getMessage();
//    header('Location: register-form.php?error=' . $e->getMessage());
    die;
} finally {
    $db = null;
}

redirectTo('login', 'Zarejestrowano. Trwa przekierowanie...');
die;
