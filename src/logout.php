<?php

require_once './includes/session.php';

try {
    setUserStatus(0);
} catch (JsonException $e) {
    echo 'Nie udało się ustawić statusu użytkownika offline dla API';
}

session_unset();

redirectTo('login', 'Wylogowano. Trwa przekierowanie...');
