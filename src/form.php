<?php

//Login form

require_once './includes/session.php';
require_once './includes/db-connection.php';

if (isLogged()) {
    redirectTo('login', 'Błąd. Login i hasło są już przechowywane. Przekierowanie...');
    die;
}

$login = trim($_POST['login']);
$password = $_POST['password'];
//    $hash_password = password_hash($password, PASSWORD_BCRYPT);

try {
    //Empty login or password -> redirect back to the login
    if (empty($login) || empty($password)) {
        throw new Exception('Login lub hasło jest puste!');
    }

    $stmt = <<<SQL
            SELECT login, password FROM users
            WHERE login=:login AND password=:password LIMIT 1
            SQL;

    $query = $db->prepare($stmt);
    $query->bindParam("login", $login, PDO::PARAM_STR);
    $query->bindParam("password", $password, PDO::PARAM_STR);
    $query->execute();
    $data = $query->fetch();

    if (!$data) {
        throw new Exception('Zły login lub hasło!');
    }

    $key = generateKey($login, $password);
    if (!verifyLogin($login, $key)) {
        throw new LogicException('Nie zalogowano do api');
    }

} catch (LogicException $e) {
    echo $e->getMessage();
    die;
}
catch (PDOException $e) {
    echo 'Błąd bazy danych: ' . $e->getMessage();
} catch (Exception $e) {
    header('Location: login.php?error=' . $e->getMessage());
    die;
} finally {
    $db = null;
}

//Form is valid -> redirect to the index
$_SESSION['login'] = $login;
$_SESSION['password'] = $password;
$_SESSION['key'] = $key;

try {
    setUserStatus(true);
} catch (JsonException $e) {
    echo 'Nie ustawiono statusu online dla API. ' . $e->getMessage();
}

redirectTo('index', 'Podano poprawne dane. Trwa przekierowanie...');
