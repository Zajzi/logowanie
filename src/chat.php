<?php

require_once './includes/session.php';

if (!isLogged()) {
    redirectTo('login', 'Nie jesteś zalogowany. Trwa przekierowanie...');
}

$avatar = getUserAvatar($_SESSION['login']);
$placeholder = 'https://i.pinimg.com/originals/ff/a0/9a/ffa09aec412db3f54deadf1b3781de2a.png';

?>

<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Czat</title>
    <?php require_once 'libs.php' ?>
</head>
<body>

<div class="container">

    <!-- Logout -->
    <div class="row">
        <div class="col text-right">
            <label style="font-weight: bold;padding: 20px;">
                Witaj <?php echo $_SESSION['login'] ?>
            </label>
            <a href="logout.php">
                <button class="btn btn-warning text-black-50" type="submit">Wyloguj</button>
            </a>
        </div>
    </div>

    <div class="row mb-5 mt-4">
        <!-- New conversation -->
        <div class="col-sm-6 col-md-3 offset-sm-3 offset-md-1 text-center align-self-end mt-4">
            <form action="chat-query.php" method="post">
                <div class="form-group">
                    <label for="conversationName" style="font-weight: bold;">Nowa konwersacja</label>
                    <input class="form-control" type="text" id="conversationName" placeholder="Nazwa konwersacji"
                           name="conversationName">
                </div>
                <button class="btn btn-info" name="createConversation" type="submit">Utwórz konwersację</button>
            </form>
        </div>

        <!-- Avatar -->
        <div class="col-sm-6 col-md-2 offset-sm-3 offset-md-1 text-center mt-4">
            <img width="60" height="60" src="<?php echo $avatar; ?>"/>

            <form action="chat-query.php" method="post">
                <div class="form-group">
                    <label for="avatar" style="font-weight: bold;">Avatar</label>
                    <input class="form-control" type="url" id="avatar" placeholder="Link avatara" name="avatar"
                           value="<?php
                           if ($avatar != $placeholder) {
                               echo $avatar;
                           }
                           ?>">
                </div>
                <button class="btn btn-info" name="setAvatar" type="submit">Zatwierdź</button>
            </form>
        </div>

        <!-- Add user to the conversation -->
        <div class="col-sm-6 col-md-3 offset-sm-3 offset-md-1 text-center align-self-end mt-4">
            <form action="chat-query.php" method="post">
                <div class="form-group">
                    <label for="loginToAdd" style="font-weight: bold;">Dodaj do konwersacji</label>
                    <input class="form-control" type="text" id="loginToAdd" placeholder="Nazwa użytkownika"
                           name="loginToAdd">
                </div>
                <input class="form-control mb-3" type="number" placeholder="ID konwersacji" name="conversationId">
                <button class="btn btn-info" name="joinConversation" type="submit">Dodaj użytkownika</button>
            </form>
        </div>
    </div>


    <!-- Active conversations -->
    <hr style="border-top: gray 1px solid;">
    <div class="row text-center">
        <div class="col">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <caption style="caption-side: top !important;font-weight: bold !important;">Aktywne konwersacje
                    </caption>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nazwa konwersacji</th>
                        <th>Użytkownicy</th>
                        <th style="width: 1%;">Akcja</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    try {
                        $conversations = getActiveConversations();

                        foreach ($conversations as $conversation) {

                            //Prepare to show the users in the conversation
                            $htmlUsersInConversation = 'brak';
                            if ($conversation['users']) {
                                $htmlUsersInConversation = '<ul>';
                                foreach ($conversation['users'] as $userInConversation) {
                                    $htmlUsersInConversation .= '<li>' . $userInConversation . '</li>';
                                }
                                $htmlUsersInConversation .= '</ul>';
                            }

                            //Content of the conversations table
                            $html = <<<HTML
                                <tr>
                                <td class="align-middle">{$conversation['id']}</td>
                                <td class="align-middle">
                                    <a href="javascript:window.open('
                                    conversation.php?conversationId={$conversation['id']}&conversationName={$conversation['name']}',
                                    'Konwersacja','width=500,height=700')">
                                        {$conversation['name']}
                                    </a>
                                </td>
                                <td class="align-middle">{$htmlUsersInConversation}</td>
                                <td class="align-middle">
                                    <a href="chat-query.php?leaveConversationId={$conversation['id']}">
                                        Opuść konwersację
                                    </a>
                                </td>
                                </tr>
                                HTML;
                            echo $html;
                        }

                    } catch (JsonException $e) {
                        echo 'Błąd aktywnych konwersacji';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- All Users -->
    <hr style="border-top: gray 1px solid;">
    <div class="row text-center">
        <div class="col-md-8 offset-md-2">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <caption style="caption-side: top !important;font-weight: bold !important;">Użytkownicy</caption>
                    <thead>
                    <tr>
                        <th>Login</th>
                        <th>Status</th>
                        <th style="width: 1%;">Avatar</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    try {
                        $users = getAllUsers();
                    } catch (JsonException $e) {
                        echo $e->getMessage();
                        die;
                    }
                    foreach ($users as $user) {

                        $icon = $user['icon'];
                        if ($icon === '') {
                            $icon = 'https://i.pinimg.com/originals/ff/a0/9a/ffa09aec412db3f54deadf1b3781de2a.png';
                        }

                        $html = <<<HTML
                            <tr>
                            <td class="align-middle">{$user['login']}</td>
                            <td class="align-middle">{$user['status']}</td>
                            <td class="align-middle"><img src="{$icon}" width="50" height="50" /></td>
                            </tr>
                            HTML;
                        echo $html;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
</body>
</html>
