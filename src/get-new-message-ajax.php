<?php

require_once './includes/session.php';

try {
    $messages = getMessages($_POST['lastMessageId']);

    $messages = array_filter($messages, fn($value) => $value['chat_id'] == $_POST['conversationId']);

    usort($messages, fn($item1, $item2) => $item1['id'] <=> $item2['id']);

    $json = json_encode($messages, JSON_THROW_ON_ERROR, 512);

} catch (JsonException $e) {
    echo $e->getMessage();
}

echo $json;
