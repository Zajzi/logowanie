<?php

const HOST = 'localhost';
const DB_USER = 'root';
const DB_PASS = '';
const DB_NAME = 'phpcamp';
const PORT = 3307;

try {
    $db = new PDO("mysql:host=" . HOST . ";port=" . PORT . ";dbname=" . DB_NAME . "", DB_USER, DB_PASS);
} catch (PDOException $e) {
    echo 'Nieudane połączenie: ' . $e->getMessage();
}


$isAdmin = function () use ($db) {
    try {
        //Check if user is logged in
        if (!(isset($_SESSION['login']) && isset($_SESSION['password']))) {
            throw new Exception('Błąd. Sprawdź czy jesteś zalogowany.');
        }

        $query = $db->prepare('SELECT role FROM user_roles WHERE login=:login LIMIT 1');
        $query->bindParam("login", $_SESSION['login'], PDO::PARAM_STR);
        $query->execute();

        if (!$query->rowCount()) {
            throw new Exception('Nie jesteś administratorem.');
        }

    } catch (PDOException $e) {
        echo 'Błąd bazy danych: ' . $e->getMessage();
        die;
    } catch (Exception $e) {
        echo 'Błąd: ' . $e->getMessage();
        die;
    }
    return true;
};
