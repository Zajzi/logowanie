<?php

const REDIRECT_IN_SECONDS = 2;

if (isset($_COOKIE['moja_sesja'])) {
    @session_id($_COOKIE['moja_sesja']);
}

session_start();

setcookie('moja_sesja', session_id());


function isLogged(): bool
{
    return (isset($_SESSION['login'], $_SESSION['password']));
}

function redirectTo(
    string $page,
    string $msg = '',
    int $seconds = REDIRECT_IN_SECONDS): void
{
    header("refresh:" . $seconds . ";url=" . $page . ".php");
    if (!$msg) {
        die('Przekierowanie za ' . $seconds . 's...');
    }
    die($msg);
}

function debug($variable, $die = true): void
{
    echo '<pre>';
    var_dump($variable);
    echo '</pre>';
    $die ? die : '';
}

//-----------------CURL and API-------------------

function generateKey($login, $password): string
{
    return md5($login . hash('sha256', $password));
}

/**
 * @param string $endpoint
 * @param array $paramsAssoc
 * @return array
 * @throws JsonException
 */
function curl(string $endpoint, array $paramsAssoc = []): array
{
    $params = http_build_query($paramsAssoc);

    $ch = curl_init();
    $options = [
        CURLOPT_URL => $endpoint,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => (bool)$params,
        CURLOPT_POSTFIELDS => $params,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_SSL_VERIFYHOST => 0,
    ];
    curl_setopt_array($ch, $options);

    $json = curl_exec($ch);
    curl_close($ch);

    try {
        $result = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
    } catch (JsonException $e) {
        //Rethrow JsonException
        throw new JsonException('JsonException: ' . $e->getMessage()
            . '. [JsonVariable]: ' . print_r($json, true));
    }

    return $result;
}

/**
 * @return array
 * @throws JsonException
 */
function getAllUsers(): array
{
    $url = 'http://tank.iai-system.com/api/user/getAll';

    try {
        $result = curl($url);
    } catch (JsonException $e) {
        throw new JsonException($e->getMessage());
    }

    return $result;
}

/**
 * @param bool $status
 * 1 = online
 * 0 = offline
 * @return void
 * @throws JsonException
 */
function setUserStatus(bool $status): void
{
    $url = 'http://tank.iai-system.com/api/user/edit';
    $arr = [
        'login' => $_SESSION['login'],
        'key' => $_SESSION['key'],
        'status' => ($status ? 'online' : 'offline')
    ];
    try {
        curl($url, $arr);
    } catch (JsonException $e) {
        throw new JsonException($e->getMessage());
    }
}

function verifyLogin(string $login, string $key): bool
{
    $url = 'http://tank.iai-system.com/api/user/verify';
    $arr = [
        'login' => $login,
        'key' => $key
    ];

    try {
        $result = curl($url, $arr);
    } catch (JsonException $e) {
        return false;
    }

    if ($result['status'] === 'ok') {
        return true;
    }
    return false;
}

/**
 * @return array
 * @throws JsonException
 */
function getActiveConversations(): array
{
    $url = 'http://tank.iai-system.com/api/chat/getActive';
    $arr = [
        'login' => $_SESSION['login'],
        'key' => $_SESSION['key']
    ];

    try {
        $result = curl($url, $arr);
    } catch (JsonException $e) {
        throw new JsonException($e->getMessage());
    }

    return $result;
}

/**
 * @param int $conversationId
 * @return array
 */
function getUsersInConversation(int $conversationId): ?array
{
    try {
        $conversations = getActiveConversations();
    } catch (JsonException $e) {
        echo $e->getMessage();
        return [];
    }

    $conversation = array_filter($conversations, fn($value) => $value['id'] == $conversationId);

    //If users exists
    if ($conversation) {
        $conversation = array_values($conversation);
        return $conversation[0]['users'];
    }
    return null;
}

/**
 * @param string $conversationName
 * @return int
 * @throws JsonException
 */
function createConversation(string $conversationName): int
{
    $url = 'http://tank.iai-system.com/api/chat/create';
    $arr = [
        'login' => $_SESSION['login'],
        'key' => $_SESSION['key'],
        'name' => $conversationName
    ];
    try {
        $result = curl($url, $arr);
    } catch (JsonException $e) {
        throw new JsonException($e->getMessage());
    }

    if ($result['status'] === 'ok') {
        return $result['id'];
    } else {
        throw new LogicException('Nie udało się utworzyć rozmowy');
    }
}

/**
 * @param string $loginToAdd
 * @param int $conversationId
 * @return void
 * @throws JsonException|LogicException
 */
function joinConversation(string $loginToAdd, int $conversationId): void
{
    $url = 'http://tank.iai-system.com/api/chat/join';
    $arr = [
        'login' => $_SESSION['login'],
        'key' => $_SESSION['key'],
        'user' => $loginToAdd,
        'chat_id' => $conversationId
    ];

    try {
        $result = curl($url, $arr);
    } catch (JsonException $e) {
        throw new JsonException($e->getMessage());
    }

    if ($result['status'] !== 'ok') {
        throw new LogicException('Nie udało się dołączyć do rozmowy');
    }
}

/**
 * @param int $conversationId
 * @return void
 * @throws JsonException
 */
function leaveConversation(int $conversationId): void
{
    $url = 'http://tank.iai-system.com/api/chat/leave';
    $arr = [
        'login' => $_SESSION['login'],
        'key' => $_SESSION['key'],
        'chat_id' => $conversationId
    ];

    try {
        $result = curl($url, $arr);
    } catch (JsonException $e) {
        throw new JsonException($e->getMessage());
    }

    if ($result['status'] !== 'ok') {
        throw new LogicException('Nie udało się opuścić rozmowy');
    }
}

/**
 * @param int $conversationId
 * @param string $msg
 * @return mixed
 * @throws JsonException
 */
function sendMessage(int $conversationId, string $msg): int
{
    $url = 'http://tank.iai-system.com/api/chat/send';
    $arr = [
        'login' => $_SESSION['login'],
        'key' => $_SESSION['key'],
        'chat_id' => $conversationId,
        'message' => $msg
    ];

    try {
        $result = curl($url, $arr);
    } catch (JsonException $e) {
        throw new JsonException($e->getMessage());
    }

    if ($result['status'] !== 'ok') {
        throw new LogicException('Nie udało się wysłać wiadomości');
    }

    return $result['id'];
}

/**
 * @param $lastId
 * @return array
 * @throws JsonException|LogicException
 */
function getMessages(int $lastId = 0): array
{
    $url = 'http://tank.iai-system.com/api/chat/get';
    $arr = [
        'login' => $_SESSION['login'],
        'key' => $_SESSION['key'],
        'last_id' => $lastId
    ];

    try {
        $result = curl($url, $arr);
    } catch (JsonException $e) {
        throw new JsonException($e->getMessage());
    }

    if ($result['status'] !== 'ok') {
        throw new LogicException('Nie udało się pobrać rozmów.');
    }

    return $result['list'];
}

/**
 * @param string $login
 * @return string
 */
function getUserAvatar(string $login): string
{
    $avatar = 'https://i.pinimg.com/originals/ff/a0/9a/ffa09aec412db3f54deadf1b3781de2a.png';

    try {
        $users = getAllUsers();
    } catch (JsonException $e) {
        return $avatar;
    }

    $user = array_filter($users, fn($value) => $value['login'] === $login);

    //If users exists
    if ($user) {
        $user = array_values($user);

        if ($user[0]['icon'] !== '') {
            $avatar = $user[0]['icon'];
        }
    }

    return $avatar;
}

/**
 * @param string $link
 * @throws JsonException
 */
function setUserAvatar(string $link): void
{
    $url = 'http://tank.iai-system.com/api/user/edit';
    $arr = [
        'login' => $_SESSION['login'],
        'key' => $_SESSION['key'],
        'icon' => $link
    ];
    try {
        curl($url, $arr);
    } catch (JsonException $e) {
        throw new JsonException($e->getMessage());
    }
}

function isUserOnline(string $login): bool
{
    $status = '';
    try {
        $users = getAllUsers();
    } catch (JsonException $e) {
        return false;
    }

    $user = array_filter($users, fn($value) => $value['login'] === $login);
    if ($user) {
        $user = array_values($user);
        $status = $user[0]['status'];
    }

    if ($status === "online") {
        return true;
    }

    return false;
}
