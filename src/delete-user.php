<?php

use Zad3\Classes\Database;
use Zad3\Classes\UserDAO;

require_once '../vendor/autoload.php';
require_once './includes/session.php';

$db = new Database();
$userDAO = new UserDAO($db);

if (!$db->isAdmin()) {
    redirectTo('index', 'Brak uprawnień do przeglądania tej strony. Trwa przekierowanie...');
    die;
}

$userToDelete = $_GET['login'];

try {
    //Delete user
    $userDAO->deleteUser($userToDelete);

} catch (PDOException $e) {
    redirectTo('index', $e->getMessage() . ' Trwa przekierowanie...');
} catch (Exception $e) {
    redirectTo('index', $e->getMessage() . ' Trwa przekierowanie...');
} finally {
    $db = null;
}

//Self-delete
if ($_SESSION['login'] === $userToDelete) {
    session_unset();
}

redirectTo('index', 'Usunięto użytkownika <b>' . $userToDelete . '</b>');
