<?php

use Zad3\Classes\Database;
use Zad3\Classes\UserDAO;

require_once '../vendor/autoload.php';
require_once './includes/session.php';

//Simple access restriction for requests other than POST
if (empty($_POST) || !isLogged()) {
    redirectTo('index', 'Nieuprawniony dostęp. Trwa przekierowanie...');
    die;
}

$db = new Database();
$userDAO = new UserDAO($db);

$login = $_POST['login'];
$oldLogin = $_POST['old-login'];
$password = $_POST['password'];

try {
    //Empty login or password -> redirect back to the index
    if (empty($login) || empty($password)) {
        throw new Exception('Login lub hasło jest puste!');
    }

    //The passwords don't match
    if ($password !== $_POST['password-repeat']) {
        throw new Exception('Podane hasła nie są identyczne!');
    }

    try {
        $result = $userDAO->editUser(
            $login,
            $password,
            $_POST['email'],
            $_POST['age'],
            $_POST['phone'],
            $_POST['city'],
            $oldLogin
        );
    } catch (PDOException $e) {
        //Re-throw
        throw new PDOException($e->getMessage());
    }

    if ($result) {
        if ($_SESSION['login'] === $oldLogin) {
            $_SESSION['login'] = $login;
            $_SESSION['password'] = $password;
        }
    } else {
        throw new Exception('Nie udało się zapisać.');
    }

} catch (PDOException $e) {
    echo 'Błąd bazy danych: ' . $e->getMessage();
} catch (Exception $e) {
    header('Location: edit-user.php?error=' . $e->getMessage() . '&login=' . $oldLogin);
    die;
} finally {
    $db = null;
}

redirectTo('index', 'Zapisano. Trwa przekierowanie...');
