<?php

use Zad3\Classes\Database;
use Zad3\Classes\UserDAO;

require_once '../vendor/autoload.php';
require_once './includes/session.php';
//require_once './includes/db-connection.php';

$db = new Database();
$userDAO = new UserDAO($db);
$userToAdmin = $_GET['login'];

if (!$db->isAdmin()) {
    die;
}

try {
    //Make user as admin
    $user = $userDAO->getUser($userToAdmin);
    $userDAO->addAdmin($user);
} catch (PDOException $e) {
    redirectTo('index', 'Nie przyznano uprawnień. Być może ten użytkownik jest już adminem.');
} catch (Exception $e) {
    redirectTo('index', $e->getMessage() . ' Trwa przekierowanie...');
} finally {
    $db = null;
}

redirectTo('index', 'Nadano uprawnienia admina użytkownikowi <b>' . $userToAdmin . '</b>');
