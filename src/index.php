<?php

require_once '../vendor/autoload.php';
require_once './includes/session.php';

use Zad3\Classes\Database;
use Zad3\Classes\UserDAO;

$db = new Database();
$userDAO = new UserDAO($db);
//User isn't logged in. Redirect to the login
if (!$db->userIsLogged()) {
    redirectTo('login');
    die;
}

?>

<!DOCTYPE html>
<html lang="pl">
<style>
    table, th, td {
        border: gray 1px solid;
    }
    th, td {
        padding:15px;
    }
</style>
<body>

<br />
<span style="font-size: 1.25em;">
    Witaj. Jesteś zalogowany jako <strong><?php echo $_SESSION['login']; ?></strong>
</span><br /><br />
<form action="logout.php" method="post">
    <input type="submit" value="Wyloguj" style="width: 6em; height: 3em"/>
</form>

<br /><hr /><br />
<a href="chat.php" style="font-size: 130%">>> Czat <<</a><br /><br />
<hr />
<strong>Lista użytkowników:<br /><br /></strong>
<table><th>Login</th><th>Email</th><th>Rola</th><th colspan="3">Akcje</th>

<?php

$users = $userDAO->getAllUsers(true);

foreach ($users as $user) {
        $userRole = $user['role'] ?? 'użytkownik';
        echo '<tr>';
        echo <<<ROW
                <td>{$user['login']}</td>
                <td>{$user['email']}</td>
                <td>{$userRole}</td>
                <td><a href="edit-user.php?login={$user['login']}">Edycja</a></td>
                <td><a href="delete-user.php?login={$user['login']}">Usuń</a></td>
                <td><a href="add-admin.php?login={$user['login']}">Nadaj admina</td>
                ROW;
        echo '</tr>';
    }
?>

</table>
</body>
</html>
