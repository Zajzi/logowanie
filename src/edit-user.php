<?php

require_once './includes/session.php';
require_once './includes/db-connection.php';

if (!isLogged()) {
    redirectTo('login', 'Jesteś niezalogowany. Trwa przekierowanie...');
    die;
}

try {
    //Get user
    $stmt = "SELECT * FROM users WHERE login=:login LIMIT 1";
    $query = $db->prepare($stmt);
    $query->bindParam("login", $_GET['login'], PDO::PARAM_STR);
    $query->execute();
    $user = $query->fetch();
} catch (PDOException $e) {
    echo 'Błąd bazy danych: ' . $e->getMessage();
} finally {
    $db = null;
}

?>

<!DOCTYPE html>
<html lang="pl">
<body style="margin: 20px auto; width: 85%; font-size: 125%">

<form action="edit-user-process.php" method="post">
    <?php
    if (isset($_GET['error'])) {
        echo '<label style="color:red;">' . $_GET['error'] . '</label><br />';
    }
    ?>
    <label for="login">Login(*):</label><br />
    <input type="text" name="login" value="<?php echo $user['login'] ?>" /><br /><br />
    <input type="hidden" name="old-login" value="<?php echo $user['login'] ?>" />

    <label for="password">Hasło(*):</label><br />
    <input type="password" name="password" value="<?php echo $user['password'] ?>" /><br /><br />

    <label for="password-repeat">Powtórz hasło(*):</label><br />
    <input type="password" name="password-repeat" value="<?php echo $user['password'] ?>" /><br /><br />

    <label for="email">Email:</label><br />
    <input type="email" name="email" value="<?php echo $user['email'] ?>" /><br /><br />

    <label for="age">Wiek:</label><br />
    <input type="number" name="age" value="<?php echo $user['age'] ?>" /><br /><br />

    <label for="phone">Telefon:</label><br />
    <input type="tel" name="phone" value="<?php echo $user['phone'] ?>" /><br /><br />

    <label for="city">Miejscowość:</label><br />
    <input type="text" name="city" value="<?php echo $user['city'] ?>" /><br /><br />

    <span style="font-size: 70%">Pola oznaczone gwiazdką (*) są obowiązkowe.</span><br /><br />
    <input type="submit" value="Zapisz" style="width: 8em; height: 3em" />
</form>

</body>
</html>
