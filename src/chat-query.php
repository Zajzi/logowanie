<?php

require_once './includes/session.php';

if ($_GET) {
    if (isset($_GET['leaveConversationId'])) {
        try {
            leaveConversation((int)$_GET['leaveConversationId']);
            if (isset($_GET['conversationWindow'])) {
                echo "<script>window.close();</script>"; //Close the conversation window on the success
                die;
            }
            redirectTo('chat', 'Opuszczono konwersację');
        } catch (JsonException $e) {
            echo 'Nie udało się opuścić konwersacji ' . $_GET['leaveConversationId'];
        }
    }
}

if ($_POST) {

    if (isset($_POST['getUserAvatar'])) {
        echo getUserAvatar($_POST['login']);
        die;
    }

    if (isset($_POST['createConversation'])) {
        try {
            redirectTo(
                'chat',
                'ID konwersacji: ' . createConversation($_POST['conversationName']),
                4);
        } catch (JsonException $e) {
            echo $e->getMessage();
            die;
        }
    } elseif (isset($_POST['joinConversation'])) {
        try {
            joinConversation($_POST['loginToAdd'], $_POST['conversationId']);
            redirectTo('chat', 'Dodano do konwersacji', 3);
        } catch (JsonException $e) {
            echo $e->getMessage();
            die;
        }

        //Send message by vanilia PHP
//    } elseif (isset($_POST['sendMessage'])) {
//        try {
//            sendMessage((int) $_POST['conversationId'], $_POST['message']);
//            header('Location: conversation.php?conversationId=' . $_POST['conversationId']);
//            die;
//        } catch (JsonException $e) {
//            echo $e->getMessage();
//            die;
//        }

    } elseif (isset($_POST['setAvatar'])) {
        try {
            setUserAvatar($_POST['avatar']);
            redirectTo('chat', 'Ustawiono avatar.');
        } catch (JsonException $e) {
            redirectTo('chat', 'Nie udało się ustawić avataru.');
        }
    } else {
        echo "Błąd";
    }
}
