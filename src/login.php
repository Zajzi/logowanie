<?php

require_once '../vendor/autoload.php';
require_once './includes/session.php';

use Zad3\Classes\Database;

$db = new Database();

//User is logged in. Redirect to the index
if ($db->userIsLogged()) {
    redirectTo('index');
    die;
}

?>

<!DOCTYPE html>
<html lang="pl">
<body style="margin: 50px auto; width: 85%; font-size: 150%">

<form action="form.php" method="post">
    <?php
    if (isset($_GET['error'])) {
        echo '<label style="color:red;">' . $_GET['error'] . '</label><br />';
    }
    ?>
    <label for="login">Login:</label><br />
    <input type="text" name="login" /><br /><br />

    <label for="password">Hasło:</label><br />
    <input type="password" name="password" /><br /><br />

    <input type="submit" value="Zaloguj" style="width: 6em; height: 3em" />
</form>

<br /><hr><br />

<a href="register-form.php">Rejestracja</a>

</body>
</html>
