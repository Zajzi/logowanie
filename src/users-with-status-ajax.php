<?php

require_once './includes/session.php';

$users = getUsersInConversation((int) $_POST['conversationId']);

//Create associate array 'login' => 'status'
$usersWithStatus = [];

if ($users) {
    foreach ($users as $user) {
        $usersWithStatus[$user] = isUserOnline($user);
    }
}

try {
    $json = json_encode($usersWithStatus, JSON_THROW_ON_ERROR, 512);
} catch (JsonException $e) {
    echo $e->getMessage();
}

echo $json;
