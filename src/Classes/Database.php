<?php

namespace Zad3\Classes;

use Exception;
use PDO;
use PDOException;

class Database
{
    private PDO $db;

    public function __construct()
    {
        $host = 'localhost';
        $dbUser = 'root';
        $dbPass = '';
        $dbName = 'phpcamp';
        $port = 3307;

        try {
            $this->db = new PDO("mysql:host=" . $host . ";port=" . $port . ";dbname=" . $dbName . "", $dbUser, $dbPass);
        } catch (PDOException $e) {
            echo 'Nieudane połączenie: ' . $e->getMessage();
        }
    }

    /**
     * @param string $stmt
     * @param mixed ...$vars
     * @return array
     * @throws PDOException if operation fail
     */
    public function query(string $stmt, ...$vars): array
    {
        $executeArray = [];
        foreach ($vars as $var) {
            $executeArray[] = $var;
        }
        $query = $this->db->prepare($stmt);
        if ($query->execute($executeArray)) {
            return $query->fetchAll();
        }
        throw new PDOException('Błąd zapytania bazy');
    }

    public function insert(string $stmt, ...$vars): bool
    {
        $executeArray = [];
        foreach ($vars as $var) {
            $executeArray[] = $var;
        }
        $query = $this->db->prepare($stmt);
        return $query->execute($executeArray);
    }

    public function update(string $stmt, ...$vars): bool
    {
        return $this->insert($stmt, ...$vars);
        //log
    }

    /**
     * @param string $stmt
     * @param mixed ...$vars
     * @throws PDOException
     */
    public function delete(string $stmt, ...$vars): void
    {
        $executeArray = [];
        foreach ($vars as $var) {
            $executeArray[] = $var;
        }
        $query = $this->db->prepare($stmt);
        $query->execute($executeArray);
        if (!$query->rowCount()) {
            throw new PDOException('Usunięcie nie powiodło się.');
        }
    }

    /**
     * @return PDO
     */
    public function getDb(): PDO
    {
        return $this->db;
    }

    public function isAdmin(): bool
    {
        try {
            //Check if user is logged in
            if (!(isset($_SESSION['login'], $_SESSION['password']))) {
                throw new Exception('Błąd. Sprawdź czy jesteś zalogowany.');
            }

            $query = $this->db->prepare('SELECT role FROM user_roles WHERE login=:login LIMIT 1');
            $query->bindParam("login", $_SESSION['login'], PDO::PARAM_STR);
            $query->execute();

            if (!$query->rowCount()) {
                throw new Exception('Nie jesteś administratorem.');
            }

        } catch (PDOException $e) {
            echo 'Błąd bazy danych: ' . $e->getMessage();
            die;
        } catch (Exception $e) {
            echo 'Błąd: ' . $e->getMessage();
            die;
        }
        return true;
    }

    public function userIsLogged(): bool
    {
        return isset($_SESSION['login'], $_SESSION['password']);
    }
}
