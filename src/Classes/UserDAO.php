<?php


namespace Zad3\Classes;


use PDOException;

class UserDAO
{
    private Database $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * @param bool $withRole
     * @return array|null
     * @throws PDOException
     */
    public function getAllUsers(bool $withRole = false): ?array
    {
        if ($withRole) {
            $stmt = <<<SQL
                SELECT users.login, users.email, user_roles.role
                FROM users
                LEFT JOIN user_roles ON users.login = user_roles.login;
                SQL;
        } else {
            $stmt = "SELECT login, email FROM users";
        }
        //Get the users from the database
        try {
            $result = $this->db->query($stmt);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage());
        }
        return $result;
    }

    /**
     * @param User $user
     * @throws PDOException
     */
    public function addUser(User $user): void
    {
        $stmt = "INSERT INTO users (login, password, email, age, phone, city) VALUES (?, ?, ?, ?, ?, ?)";
        $result = $this->db->insert(
            $stmt,
            $user->getLogin(),
            $user->getPassword(),
            $user->getEmail(),
            $user->getAge(),
            $user->getPhone(),
            $user->getCity()
        );
        if (!$result) {
            throw new PDOException('Błąd dodania nowego użytkownika w bazie: ');
        }
    }

    /**
     * @param array $user
     * @throws PDOException
     */
    public function addAdmin(array $user): void
    {
        $stmt = "INSERT INTO user_roles (login, role) VALUES (?, ?)";
        $result = $this->db->insert($stmt, $user['login'], 'admin');
        if (!$result) {
            throw new PDOException('Błąd bazy danych.');
        }
    }

    /**
     * @param string $login
     * @return array|null
     * @throws PDOException
     */
    public function getUser(string $login): ?array
    {
        $result = [];
        $stmt = "SELECT * FROM users WHERE login=? LIMIT 1";
        try {
            $result = $this->db->query($stmt, $login);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $result[0];
    }
    /**
     * @param string $login
     * @throws PDOException
     */
    public function deleteUser(string $login): void
    {
        $stmt = "DELETE FROM users WHERE login=? LIMIT 1";
        try {
            $this->db->delete($stmt, $login);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param mixed ...$vars
     * @return bool
     * @throws PDOException
     */
    public function editUser(...$vars): bool
    {
        $stmt = "UPDATE users SET login=?, password=?, email=?, age=?, phone=?, city=?
            WHERE login=? LIMIT 1";

        $result = $this->db->update($stmt, ...$vars);
        if (!$result) {
            throw new PDOException('Błąd bazy danych.');
        }
        return $result;
    }

}
