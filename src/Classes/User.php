<?php

namespace Zad3\Classes;


class User
{
    private string $login;
    private string $password;
    private string $email;
    private int $age;
    private string $phone;
    private string $city;
    private string $role;

    /**
     * User constructor.
     * @param string $login
     * @param string $password
     * @param string $email
     * @param string $age
     * @param string $phone
     * @param string $city
     */
    public function __construct(string $login, string $password, string $email, string $age, string $phone, string $city)
    {
        $this->login = $login;
        $this->password = $password;
        $this->email = $email;
        $this->age = (int) $age;
        $this->phone = $phone;
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
//        $db = new Database();
//        return $db->getUserRole($this->login);
        return '';
    }
}
