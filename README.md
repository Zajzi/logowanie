## Zadanie 1

1. Zrobić formularz logowania.
2. Dane do logowania maja być zapisane w pliku.
3. Dodatkowo 'Wyloguj' (usunięcie danych z sesji).

### Dane

Login: test  
Hasło: 123

## Zadanie 2

1. Zrobić panel administracyjny:
- Lista użytkowników
- Edycja użytkowników  
- Usunięcie użytkowników  
Dodatkowo:
    - Uprawnienia
    - PDO opcjonalnie
